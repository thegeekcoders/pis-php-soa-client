<?php

require __DIR__ . '/vendor/autoload.php';
session_start();

if(!isset($_SESSION['access_token'])) {
  header('Location: login.php');
  exit();
}

$client = new GuzzleHttp\Client();
$access_token = $_SESSION['access_token'];
$options = [
   'headers' => [
        "Authorization" => "Bearer " . $access_token
      ]
  ]; 
 $response = $client->request('GET', 'http://localhost:63514/api/hrEmployee', $options);
 $response_body = $response->getBody()->getContents(); 
 $json_data = json_decode($response_body,true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PIS-SOA Home</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <style>
    .container {
      display: flex;
      flex-wrap: wrap;
    }
  </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="./login.php">SOA PHP Client</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="./home.php">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./user-detail.php">User Detail</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./logout.php">Logout</a>
      </li>
    </ul>
  </div>
</nav>

<h2 class="text-center mt-3">List Of Employees</h2>
<div class="container" style='margin-top: 3rem;'>
<?php foreach($json_data  as $employee) { ?>
   <div class="card" style="width: 18rem; margin: 1rem;">
   <img src='<?php echo 'http://pis.shangrilagroup.com.np/' . $employee['photo_path'] ?>' class="card-img-top" alt="<?php echo $employee['full_name_np'] ?>">
   <div class="card-body">
     <p class="card-text"><?php echo $employee['full_name_np'] ?></p>
     <p class="card-text">Office Name: <?php echo $employee['current_office_name'] ?></p>
     <p class="card-text">Position: <?php echo $employee['position_name'] ?></p>
   </div>
   </div>

<?php } ?>
</div>
</body>
</html>
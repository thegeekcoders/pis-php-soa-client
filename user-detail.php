<?php
require __DIR__ . '/vendor/autoload.php';

use Lcobucci\JWT\Parser;

$user_detail = [];

session_start();

if(!isset($_SESSION['access_token'])) {
  header('Location: login.php');
  exit();
}
else {
  $jwt = $_SESSION['access_token'];
  $decodedToken = (new Parser())->parse((string) $jwt);

  $user_detail['username'] =$decodedToken->getClaim('username');
  $user_detail['name_en'] =$decodedToken->getClaim('name_en');
  $user_detail['role'] =$decodedToken->getClaim('role');

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PIS-SOA PHP Client</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="./login.php">SOA PHP Client</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="./home.php">Home</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="./user-detail.php">User Detail</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./logout.php">Logout</a>
      </li>
    </ul>
  </div>
</nav>
<div class="container"  style='margin-top: 3rem;'>
    <div class="card">
    <div class="card-header">
     <h1>User Details</h1>
    </div>
    <div class="card-body">
      <h3>Username: <?php echo $user_detail['username']; ?></h3>
      <h3>Name: <?php echo $user_detail['name_en']; ?></h3>
      <h3>Role: <?php echo $user_detail['role']; ?></h3>
      </div>
      </div>
</div>
</body>
</html>
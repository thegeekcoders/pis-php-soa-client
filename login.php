  <?php

  session_start();

  $flag = true;
  if(isset($_SESSION['access_token'])) {
    $flag = false;
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PIS-SOA PHP Client</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="./login.php">SOA PHP Client</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
    <?php 
      if($flag) {
     ?>
<li class="nav-item active">
        <a class="nav-link" href="./login.php">Login</a>
      </li>
      <?php } else { ?>
        <li class="nav-item">
        <a class="nav-link" href="./logout.php">Logout</a>
      </li>
      <?php } ?>
      <li class="nav-item">
        <a class="nav-link" href="./home.php">Home</a>
      </li>
    </ul>
  </div>
</nav>
<div class="container"  style='margin-top: 3rem;'>
<form method="post" action="./login_process.php">
  <div class="form-group">
    <label for="exampleInputEmail1">Username</label>
    <input type="text" class="form-control" name="username"  placeholder="Enter Username">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" name="password"  placeholder="Enter Password">
  </div>
  <input name="submit" value="Login" type="submit" class="btn btn-primary" />
</form>
</div>
</body>
</html>
<?php
  require __DIR__ . '/vendor/autoload.php';

  function getReqParam($param) {
    return $_POST[$param];
  }

  if(getReqParam('submit')) {

    $username = getReqParam('username');
    $password = getReqParam('password');

    $client = new GuzzleHttp\Client();
   $options = [
      'json' => [
          "client_id" => 1,
          "username" => $username,
          "password" => $password
         ]
     ]; 
    $response = $client->request('POST', 'http://localhost:59239/api/auth/login', $options);
    $response_body = $response->getBody()->getContents(); 
    $json_data = json_decode($response_body,true);
    session_start();
    $_SESSION['access_token'] =  $json_data['access_token'];
    header('location: home.php');
  }
 
?>